<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <link rel= "stylesheet" href= "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

    <title>South African Holidays</title>
  </head>
  <body>
    <div class="container">
        <div class="px-4 py-5 my-5 text-center">
            <h1 class="display-5 fw-bold">South African Holidays</h1>
            @if (empty($holidays))
                <div>The holiday database is currently empty</div>
            @endif
            <div>Enter a year below to fetch holidays for that year</div>
                <form method="POST" action="/holidays/fetch">
                
                @csrf

                <div class="d-flex justify-content-center mt-3">
                    <div id='dateTimeContainer' style="position:relative">
                    <input type="text" class="form-control" name="year" id="datepicker" required autocomplete="off" />
                    </div>
                </div>
                <div class='mt-3 mb-3'><button type="submit" class="btn btn-primary">Fetch holidays</button></div>
                </form>

            <div class="row">
                <div class="col-4">
                    <div class="list-group" id="list-tab" role="tablist">
                    @php
                        $active = true
                    @endphp

                    @foreach ($holidays as $year => $month_day_desc)
                        @if ($active)
                            <a class="list-group-item list-group-item-action active" id="list-{{$year}}-list" data-bs-toggle="list" href="#list-{{$year}}" role="tab" aria-controls="list-{{$year}}">{{$year}}</a>
                            @php  
                                $active = false 
                            @endphp
                        @else
                            <a class="list-group-item list-group-item-action" id="list-{{$year}}-list" data-bs-toggle="list" href="#list-{{$year}}" role="tab" aria-controls="list-{{$year}}">{{$year}}</a>
                        @endif

                    @endforeach
                    </div>
                    @if (!empty($holidays))
                    <div class="row  mt-3">
                        <div class="d-flex justify-content-center">
                            <a class="btn btn-primary" href="{{ URL::to('/holidays/pdf') }}">Export to PDF</a>
                        </div>
                    </div>
                    @endif
                </div>
                
                <div class="col-8">
                    <div class="tab-content" id="nav-tabContent">
                    @php
                        $active = true
                    @endphp

                    @foreach ($holidays as $year => $month_day_desc)
                        @if ($active)
                            <div class="tab-pane fade show active" id="list-{{ $year }}" role="tabpanel" aria-labelledby="list-{{ $year }}-list">
                        @php  
                            $active = false 
                        @endphp
                        @else
                            <div class="tab-pane fade show" id="list-{{ $year }}" role="tabpanel" aria-labelledby="list-{{ $year }}-list">
                        @endif

                                <table class="table table-bordered table-striped mb-5">
                                    <thead>
                                        <tr class="table-dark">
                                            <th scope="col">Weekday</th>
                                            <th scope="col">Month</th>
                                            <th scope="col">Day</th>
                                            <th scope="col">Holiday</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <tbody>
                                        @foreach ($month_day_desc as $item)
                                        <tr>
                                            <td>{{ $item['weekday'] }}</td>
                                            <td>{{ $item['month'] }}</td>
                                            <td>{{ $item['day'] }}</td>
                                            <td>{{ $item['desc'] }}</td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>

                            </div>
                    @endforeach
                    </div><!-- tab-content -->
                </div> <!-- /col -->
            </div><!-- /row -->
        </div><!-- /text-center -->
    </div><!-- /container -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script type= "text/javascript" src= "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    
    <script>
    $(function(){
        $("#datepicker").datepicker({
            container:'#dateTimeContainer',
            format: "yyyy",
            viewMode: "years", 
            minViewMode: "years",
            autoclose:true,
        });
    })
    </script>
    
    </body>
</html>
