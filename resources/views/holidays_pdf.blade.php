<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    <title>South African Holidays</title>
  </head>
  <body>
    <div class="container">
        <div class="px-4 py-5 my-5 text-center">
            <h1 class="display-5 fw-bold">South African Holidays</h1>

            <div class="row">
                <div class="col-12">
                        @foreach ($holidays as $year => $month_day_desc)
                            <h2>{{ $year }}</h2>
                            <table class="table table-bordered table-striped mb-5">
                                <thead>
                                    <tr class="table-light">
                                        <th scope="col">Weekday</th>
                                        <th scope="col">Month</th>
                                        <th scope="col">Day</th>
                                        <th scope="col">Holiday</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <tbody>
                                    @foreach ($month_day_desc as $item)
                                    <tr>
                                        <td>{{ $item['weekday'] }}</td>
                                        <td>{{ $item['month'] }}</td>
                                        <td>{{ $item['day'] }}</td>
                                        <td>{{ $item['desc'] }}</td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        @endforeach
                </div><!-- /col -->
            </div><!-- /row -->
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        </div><!-- /text-center -->
    </div><!-- /container -->
  </body>
</html>
