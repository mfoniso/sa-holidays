<?php

namespace Tests\Unit;

use Tests\TestCase;
use Symfony\Component\Console\Exception\RuntimeException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Holiday;

class HolidaysTest extends TestCase
{

    use RefreshDatabase;

    public function test_calling_without_year_argument()
    {
        $this->expectException(RuntimeException::class);
        $this->artisan('holiday:fetch');
    }

    public function test_that_command_checks_existing_database_entries(){
        $holiday = Holiday::factory()->create();
        $this->artisan("holiday:fetch $holiday->year")->expectsOutput("$holiday->year holidays already exist in the database");
    }


    public function test_that_command_saves_data_to_database(){
        $year = 2022;
        $this->artisan("holiday:fetch $year");
        $this->assertNotEquals(0, Holiday::where('year', $year)->count());
    }
}
