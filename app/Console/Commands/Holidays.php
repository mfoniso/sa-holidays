<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\Holiday;
use Illuminate\Support\Facades\DB;

class Holidays extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'holiday:fetch {year}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch a list of South African holidays from external API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $year = $this->argument('year');

        $yearExists = Holiday::where('year', $year)->count();

        if ($yearExists){
            $this->info("$year holidays already exist in the database");
            return Command::SUCCESS;
        }

        $api = "https://kayaposoft.com/enrico/json/v2.0/?action=getHolidaysForYear&year=${year}&country=za&holidayType=public_holiday";

        $response = Http::get($api);

        if (! $response->successful()){
            $this->error("An error occured while contacting the API");
            return 1;
        }
        
        $holidays = $response->json();

        if (array_key_exists('error', $holidays)){
            $this->error("API returned an error: " . $holidays['error']);
            return Command::FAILURE;
        }

        foreach ($holidays as $h){
            $d = $h['date'];

            $holidayObj = new Holiday;

            $holidayObj->day = $d['day'];
            $holidayObj->month = $d['month'];
            $holidayObj->year = $d['year'];
            $holidayObj->dayOfWeek = $d['dayOfWeek'];
            $holidayObj->description = $h['name'][0]['text'];
            
            $holidayObj->save();
        }

        return Command::SUCCESS;
    }
}
