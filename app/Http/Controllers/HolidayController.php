<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Holiday;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

use PDF;

class HolidayController extends Controller
{
    /**
     * Display list of holiday from the database
     *
     * @return int
     */
    public function index(){
        $holidays = $this->get_holidays();
        return view('holidays', [ 'holidays' => $holidays]);
    }

    /**
     * Run the console command to fetch holidays for the specified year
     */
    public function fetch(Request $request){
        Artisan::call("holiday:fetch $request->year");
        return redirect('/holidays/');
    }

    /**
     * Convert the holiday listing to PDF and trigger a file download
     */
    public function exportPDF(){
        $holidays = $this->get_holidays();
        
        $pdf = PDF::loadView('holidays_pdf', ['holidays' => $holidays]);
        return $pdf->download('South_African_Holidays.pdf');
    }

    /**
     * Get holiday data from the database 
     */
    private function get_holidays(){
        $holidays = Holiday::select(['day', 'month', 'year', 'description', 'dayOfWeek'])->orderBy('year')->orderBy('month')->orderBy('day')->get();
        // Create a data structure to group holidays by year
        $grouped_by_year = [];

        foreach ($holidays as $h){
            $month = date('F', mktime(0, 0, 0, $h->month, 10));
            $weekday = date('D', strtotime("Sunday +{$h->dayOfWeek} days"));
            $grouped_by_year[$h->year][] = ['day'=>$h->day, 'month'=>$month, 'desc'=>$h->description, 'weekday'=>$weekday];
        }

        return $grouped_by_year;
    }
}
